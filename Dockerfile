FROM webdevops/php-apache-dev:ubuntu-14.04

ENV PHP_DATE_TIMEZONE="America/Sao_Paulo"
ENV WEB_DOCUMENT_ROOT=/var/www
ENV PHP_DISPLAY_ERRORS=0
ENV VIRTUAL_PORT: 80
ENV WEB_ALIAS_DOMAIN="localhost"
ENV php.short_open_tag="on"

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y php5-sybase freetds-common libsybdb5 php5-mssql php5-ldap libapache2-mod-php5 php5-mcrypt && php5enmod mcrypt

WORKDIR /var/www
